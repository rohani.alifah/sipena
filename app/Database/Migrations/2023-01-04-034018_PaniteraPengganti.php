<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class PaniteraPengganti extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id'          => [
                'type'              => 'INT',
                'constraint'        => 5,
                'unsigned'          => true,
                'auto_increment'    => true
            ],
            'nama_majelis'  => [
                'type'              => 'VARCHAR',
                'constraint'        => '255'
            ],
            'nip'      => [
                'type'              => 'VARCHAR',
                'constraint'        => '255',
                'null'              =>  true
            ],
            'jabatan' => [
                'type'              => 'TEXT',
                'null'              => true,
            ],
            'satuan_kerja'          => [
                'type'              => 'VARCHAR',
                'constraint'        => '255',
                'default'           => 'Mahkamah Agung',
            ],
            'created_at datetime default current_timestamp',
            'updated_at datetime default current_timestamp on update current_timestamp',
        ]);

        $this->forge->addKey('id', TRUE);

        $this->forge->createTable('panitera_pengganti', TRUE);
    }

    public function down()
    {
        $this->forge->dropTable('panitera_pengganti');
    }
}
